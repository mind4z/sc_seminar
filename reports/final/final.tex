\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{placeins}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{tabularx}  % for 'tabularx' environment and 'X' column type
\usepackage{ragged2e}  % for '\RaggedRight' macro (allows hyphenation)
\newcolumntype{Y}{>{\RaggedRight\arraybackslash}X} 
\usepackage{booktabs}  % for \toprule, \midrule, and \bottomrule macros 
\newcolumntype{C}{>{\centering\arraybackslash}X}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\title{Seminar Social Computing - The Wiki Project \\ Final Report}
\author{ M. B\"{a}rtschi (*), D. Moser, T. Amstutz\\ University of Bern, Switzerland}

\begin{document}
\maketitle

\section{Introduction}

The goal of our project is to provide a web service which, for any given Wikipedia article, visualizes its relevant related topics in a clear and meaningful way. For each article, we will weight the outgoing nodes by user ratings to determine their relevance. The goal is to provide users with the most important links. Users of our web service will have the possibility to choose the amount of outgoing nodes they want displayed, with a default set at five. As users can search for an Wikipedia articles (their links will be rated by other users again), the amount of Wikipedia articles and their relevant related topics will dynamically grow over time. This process might even enhance Wikipedia by adding new qualitatively important information, namely the ratings of the users.

Users can achieve rewards (medals) by writing comments and rating links. This should motivate users to not only consume our app but also to be active, so that the growth of the network is ensured. Once we have a sufficiently large user base, the quality of the links will also increase. A potential extension of the project could be to include the rating of linked images, instead of only considering articles. In this case, the web service will also display relevant images belonging to the queried Wikipedia topic.

\section{Architecture}

\subsection{Data Aquisition}

The data aquisition is achieved through a Python script using multiple Python packages. We use Flask\footnote{\url{http://flask.pocoo.org/}} to provide a RESTful API which our backend can query, the Requests\footnote{\url{http://docs.python-requests.org/en/latest/}} package to query the MediaWiki-API\footnote{\url{http://www.mediawiki.org/wiki/API:Main\_page}} of Wikipedia and SPARQLWrapper\footnote{\url{http://rdflib.github.io/sparqlwrapper/}} to query \url{dbpedia.org}. 

The exposed REST URIs by our script are:
\begin{itemize}
	\item \textbf{\texttt{/wiki/<Article Title>}} will return a list of all linked articles. Along with the title of the linked article, the script returns the amount of watchers and the length of the linked article on Wikipedia. Also, it returns if the linked article links back and the relation between the two articles if any is mentioned at the corresponding DBpedia resource entry.
	\item \textbf{\texttt{/abstract/<Article Title>}} returns the abstract of the article from DBpedia in the English language.
\end{itemize}

\subsection{Backend}

We use the Meteor\footnote{\url{http://www.meteor.com}} Javascript-Framework to build our frontend as well as our backend. Due to the special way the Meteor framework is built, the frontend and the backend are not really distinguishable anymore by means of modularity. Meteor uses NodeJS as webserver and MongoDB as Database. All topics that where searched for by users are stored in MongoDB so that the data is available quickly in future requests.

\subsection{Frontend}
The unique way this framework operates allows using MongoDB data storage not only on the server but also on the client machine. This increases the felt speed of the application. If data has been fetched once from the server, it is automatically stored on the client side. Due to that a lot less requests to the backend are necessary. Queries to downloaded parts are handled purely on the client side. Another feature that meteor offers is the client notification if parts of the DB is changed that the client is currently looking at. So if a user A looks at a topic that another user B votes on, meteor will notify client A making an update of the display possible.

Concerning the strict frontend part, we make use of state-of-the-art web development technologies like jQuery\footnote{\url{http://jquery.com/}} and Bootstrap\footnote{\url{http://getbootstrap.com/}}. To draw our graphs we rely on D3.js\footnote{\url{http://d3js.org/}} which offers a great variety of different visualization styles. D3.js allows one to bind data (most often in the .json format) to the DOM and apply data-driven transformations on it. A great benefit of D3.js is that it relays only on web standard technologies, namely HTML5, CSS3, and SVG.

\section{Installation Guide}

To run the data aquisition server script, one needs a system with Python 3 and pip installed. Optionally, to avoid installing Python packages system-wide, creating a virtual-environment (e.g. using \texttt{mkvirtualenv}) might be applicable. To install the dependencies used for the data aquisition script, enter the \texttt{wiki\_interface} folder and issue \texttt{pip install requirements.txt}. The data aquisition server then needs to be started manually with \texttt{python wiki.py}.

To run the backend server, one has to install the Meteor framework. This is done by issuing \texttt{curl https://install.meteor.com/ | sh} either on an OS X or on a Linux system. Once Meteor is available one has to enter the \texttt{meteor} folder. The backend server can be started with \texttt{meteor}. Meteor needs 1024 MB of ram to be installed and our application communicates over port 5000 (REST interface to wikipedia) and 3000 (Meteor).

\section{User Guide}

Our Wiki-Project application offers various features which help users to investigate target Wikipedia topics in an efficient, timely and user friendly way. As an anonymous user one has the possibility to search for topics of interest and access the results returned by those queries in a read only fashion. Queries can be refined by using different search modes, by setting the number of returned linked topics with respect to the target topic, and by using a specific link search filter. If a user registers for our application, he has further more the possibility to rate links (up- and downvote), to comment links, to receive life notifications when other users modify commented topics, and to access the awarding system. A complete overview for the available features is listed in Table~\ref{tab:Table1}.

\begin{table}
\footnotesize
\begin{tabularx}{\textwidth}{@{} Y C C @{}}
\toprule
\bfseries{Feature}&\bfseries{Anonymous users}&\bfseries{Registered users} \\
\midrule
Searching									&\cmark	&\cmark \\ \addlinespace
Different search modes		&\cmark	&\cmark \\ \addlinespace
Specific link search			&\cmark	&\cmark \\ \addlinespace
Tree exploring						&\cmark	&\cmark \\ \addlinespace
Live view update by up- 
and downvotes							&\cmark	&\cmark \\ \addlinespace
Up- or downvote links			&\xmark	&\cmark \\ \addlinespace
Commenting topics					&\xmark	&\cmark \\ \addlinespace
Own comments overview			&\xmark	&\cmark \\ \addlinespace
Notifications when other 
users modify topics which 
one has commented or 
rated											&\xmark	&\cmark \\ \addlinespace
Awarding system						&\xmark	&\cmark \\ \addlinespace
\bottomrule
\end{tabularx}
\caption{Overview for the available features in Wiki-Project application} 
\label{tab:Table1}
\end{table}

\subsection{Use Cases}

\paragraph{Searching and tree exploring} 
Searching for a specific topic is done by typing the topic into the search field and setting the desired options in the \textit{Advanced Settings} tab (see Figure~\ref{fig:searching}). Users have the possibility to sort links by voting and automatic ordering, by only automatic ordering, or by randomly ordering. Further they can set the number of displayed related topics, which is a number between 1 and 25. The third option \textit{Link Title} is used to restrict the link information of the related topics to the given text. The resulting tree can then be explored as following: The importance of the related topics is represented by the intensity and thickness of the links. The thicker and darker a link is, all the more important (better user rating) is the connected topic. Initially, if there is no user rating available, the link is rated by using the amount of connections from \textit{dbpedia.org}. The tree can be expanded or collapsed by using the plus or minus icons below the topic nodes.\\
To show more information for a certain topic, users have to click on the topic node, which pops up a window containing the abstract of that topic (taken from Wikipedia) and comments from other users for that topic. Further the kind of relationship between the opened topic and its parent topic is displayed next to the title. Clicking on the title of the topic opens a new browser tab with the related Wikipedia article of the topic. This summarizes the features which are available to both, the anonymous and the registered users.
\begin{figure}[htb!]
\includegraphics[scale=.4]{SE.png} 
\caption{Searching and tree exploring}
\label{fig:searching}
\end{figure}

\paragraph{Voting and commenting} 
Registered users can rate and comment topics.
\begin{wrapfigure}[15]{r}{.41\textwidth}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{VC.png}
  \end{center}
  \caption{Voting and commenting}
	\label{fig:voting}
\end{wrapfigure}
This is done by first opening the topic pop up. The comment tab can then be accessed, which displays any available comments to the related topic and a form to add more comments (see Figure~\ref{fig:voting}). If users agree with the relationship of this topic and its parent topic, they can upvote the link by clicking on the upvote button, or the downvote button respectively. Users are informed by a pop up whether the voting was successful or not. An important aspect here is that users can only vote once for a certain topic. This means that they can either up- or downvote, but not both at the same time. Further they can not up- or downvote more than once.

\paragraph{Socializing} We added several more features to enable users to socialize within the application. First, a user gets real-time notifications as soon as other users comment or rate links which he has commented or rated. Second, a user can collect points for an awarding system. Each time a comment is written or a vote is given, the users amount of points is incremented. Once the user has collected enough points, he receives a bronze, silver, or gold medal. It is obvious that comments from users with a gold medal are taken more serious than others. At last, a user can access an overview of his comments. 

\section{Experiences with the crowd}
We asked some people from our personal environment to spend some time on our application. As the amount of topics from Wikipedia is enormous and we only had a few participants, we restricted them to only use certain topics, such as \textit{World War, Germany, Hitler, etc.}, in order to get a realistic scenario for those topics. The results of the crowd simulation are listed in the following four sections.
\subsection{Motivation}
Beside the intrinsic motivation of having a powerful search engine at hand, we introduced an awarding system for those users which are very active in voting and commenting. This should motivate users to not only consume but also to give votes and comments in our application.\\
Initially, as no voting was present, the intrinsic motivation was lacking as the search results were often quite random (Wikipedia returns a lot of topics, of which some are not of interest). However, as soon as the participants have voted several times on the restricted amount of topics, they noticed that our application really helps in finding related topics for a target topic.\\
The awarding system on the other hand didn't have much impact on the participants behavior. This is most likely due to the missing visibility of the awards for other users. In the current version of our application only the user itself is able to see its award. In the next version of our application we have to display the medal next to the comments of the user so that other users are aware of it.
\subsection{Accuracy measurement}
Although we only had a small crowd using our application, the trend for a real usage became apparent. Hence we can say that searching for topics with coarse-grained ratings returns links of which the context represents strongly the view of only a few people. Depending on whether the user agrees with this context or not, the results can be suitable or not. \\
On the other hand, topics with a fine-grained rating represent the view of the crowd and the results for those topics are the most important links in an overall fashion. If a users does not agree with this context he still can use the \textit{Link filter} to restrict the results to its own target context.

\subsection{Untruthfulness}
Untruthfulness can happen in two parts of our application. First, in the voting part and second, in the commenting part. To handle untruthfulness in voting, we introduced two mechanisms. In order to be able to vote, a user has to be registered and logged in. This prevents the abuse of our applications by bots. Further, a user can only vote once for a certain topic. Therefore he can not arbitrary change the rating of a link as the impact of a single vote is not substantial. This covers untruthfulness in voting satisfactory as the simulation with the participants has shown.\\
However, comments are not post processed and users could abuse this functionality if they want. We had no problems in our simulation, but if the application goes life, we certainly would have to include some security checks for comments. 

\subsection{Lessons learned}
Beside some minor issues the main lesson we learned is that having a good application is not enough if the user basis is missing. As the simulation has shown, the usage of our application is restricted if there are only a few voting available for the topics. It is very important that such an application offers features which stimulates users to actively participate and not to only consume. 

\section{User feedback}
We asked several people from our personal environment to use our application and to give us feedback about what is useful and what could be improved. Table~\ref{tab:Table2} summarizes the feedback we received. According to those feedback the main benefit of our application is the fast and easy access to information as well as the \textit{Link filter} which enables users to restrict search results to a certain context. However, there are also several main drawbacks, such as missing rerendering the tree after changing the settings and that the search results are often not the most important ones (this will change once we have enough voting). 
\begin{table}
\footnotesize
\begin{tabularx}{\textwidth}{@{} Y Y @{}}
\toprule
\bfseries{Pros}&\bfseries{Cons} \\
\midrule
\addlinespace\addlinespace
The layout and colors are very attractive	& Changing the settings should reload the tree	\\ \addlinespace
Searching for a certain topic with default settings is accessible very fast & If a topic is loaded initially the related links are often bad \\ \addlinespace
Navigation is intuitive	& Tree should be cached on changing the tab	\\ \addlinespace
Searching and investigating topics is implemented efficiently & Initial topic load takes too long \\ \addlinespace
Powerful voting system & Awarding system does not really encourage one to participate \\ \addlinespace
Comments & One should be able to delete its own comments \\ \addlinespace
Comments overview page is very handy &\\ \addlinespace
Restrict connected results by using the Link filter is enormously helpful & \\ \addlinespace\addlinespace
\bottomrule
\end{tabularx}
\caption{User feedback} 
\label{tab:Table2}
\end{table}

\FloatBarrier
\section{Conclusion}
With our Wiki-Project application we introduce a new way of searching for specific Wikipedia topics. To facilitate finding relevant information in Wikipedias growing database, our system displays for a queried topic the abstract of the topic itself and an overview of its closest matching topics (based on user ratings). This enables users to get additional knowledge about the target topic without having to search for it manually.\\
First user feedback have shown that the idea behind our application encounters a lot of interest. The implementation itself is done quite well with some minor issues, such as automatically updating and rerendering the tree after changing the settings.\\
The main open issue is that features which stimulates users to actively participate (e.g. vote and comment) in our application are missing. This is a major problem as our application can not reveal its real power without having a basic amount of ratings available. Hence, there is some work to be done before our application is fit to go life.

\end{document}
