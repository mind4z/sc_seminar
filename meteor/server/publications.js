Meteor.publish('abstracts', function() {
    return Abstracts.find({});
});
Meteor.publish('topics', function() {
    return Topics.find({});
});
Meteor.publish('links', function() {
    return Links.find({});
});
Meteor.publish('votes', function() {
    return Votes.find({});
});
Meteor.publish('comments', function(postId) {
    return Comments.find({});
});
Meteor.publish('notifications', function() {
    return Notifications.find();
});