Package.describe({
    name: 'wiki',
    summary: "Wiki package",
    version: '1.0.0'
});

Package.onUse(function (api) {
    api.addFiles('wiki.js', 'server');
    api.export('Wiki');
});