Wiki = {
    url : "http://localhost:5000",

    getLinks : function(title){
        return this.makeCall(this.url+"/wiki/"+title,20000);
    },
    getAbstract : function(title){
        return this.makeCall(this.url+"/abstract/"+title,10000);
    },
    meteorGet : function(call,timeout,self,callback){
        Meteor.http.get(call, {timeout: timeout},function(error, result){
                if(error){
                    if(error.code=="ECONNREFUSED"){
                        callback( null,{
                            error : {
                                type: "NO_CONNECTION",
                                reason: "Connection to wiki interface on "+self.url+" not possible"
                            }
                        });
                    }else if(error.statusCode==500){
                        callback( null,{
                            error : {
                                type: "NOT_FOUND",
                                reason: "This topic has not been found on wikipedia"
                            }
                        });
                    } else{
                        callback( null,{
                            error : {
                                type: "UNKNOWN",
                                reason: "This topic has not been found on wikipedia"
                            }
                        });
                    }

                }
                else if(result.statusCode === 200){
                        callback( null,result.data);
                }else{
                    callback( null,{
                        error : {
                            type: "UNKNOWN",
                            reason: "Unknonw2 error while calling wiki interface on "+self.url
                        }
                    });
                }
            }
        )
    },
    makeCall: function(call,timeout){
        console.log(call);
        syncFunc = Meteor.wrapAsync(this.meteorGet);
        return syncFunc(call,timeout,this);
    }
};