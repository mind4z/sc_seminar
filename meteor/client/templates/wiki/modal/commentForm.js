Template.comments.events({
    'click #submit-comments': function(e) {
        e.preventDefault();

        var comment = {
            content: $('#comment-area').val()
        };

        var current_topic =  NodeModal.findOne();
        comment.topicTitle = current_topic.title;
        comment.topicParent = current_topic.parent;

        Meteor.call('addComment', comment, function(error, result) {
            if (error)
            {
                throwError(error.reason);
            }

        });

    }
});