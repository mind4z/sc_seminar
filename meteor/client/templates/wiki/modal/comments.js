Template.comments.rendered = function () {
    $('.collapse').collapse();
}

Template.comments.helpers({
    comments: function() {
        node =  NodeModal.findOne();
        if(node)
        {
            return Comments.find({topicTitle: node.title,topicParent: node.parent});
        }
        else{
            return null;
        }
    },
    user: function(){
        return Meteor.user();
    }
});