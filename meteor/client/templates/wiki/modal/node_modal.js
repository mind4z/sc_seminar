// Local (client-only) NodeModal
NodeModal = new Mongo.Collection(null);
refreshModal = function(){
    var node = NodeModal.findOne();
    if(node){
        NodeModal.remove({});
        displayModal(node);
    }
}
displayModal = function(node) {
    Meteor.call('addAbstract', node.title, function (error, result) {
        var abstract = result.abstract;
        var upvoted = false;
        var downvoted = false;
        var parent = null;
        if(node.parent && node.parent.title){
            parent = node.parent.title;
        }
        else if(node.parent){
            parent = node.parent;
        }

        Meteor.call('getUpvotedByCurrentUser',node.title,parent, function(error, result) {
            upvoted = result?true:false;
            Meteor.call('getDownvotedByCurrentUser',node.title,parent, function(error, result) {
                downvoted = result?true:false;
                NodeModal.insert({
                    title: node.title,
                    parent: parent,
                    abstract: abstract,
                    url:node.url,
                    votes:node.votes,
                    upvoted: upvoted,
                    downvoted: downvoted,
                    semantic: node.semantic == "" ? "No semantics": node.semantic
                });
                $("#node-modal").modal("show");
                $('#node-modal').on('shown.bs.modal', function (e) {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            });
        });


    });
}

Template.nodeModal.update = function() {
    $('[data-toggle="tooltip"]').tooltip();
}

Template.nodeModal.helpers({
    nodeModal: function() {
        return NodeModal.findOne();;
    },
    user: function(){
        return Meteor.user();
    },
    upvoteDeactive: function(){
        if(NodeModal.findOne({upvoted:true})){
            return "disabled";
        }
        return "";
    },
    downvoteDeactive: function(){
        if(NodeModal.findOne({downvoted:true})){
            return "disabled";
        }
        return "";
    }
});

Template.nodeModal.events({
    'hidden.bs.modal': function() {
        NodeModal.remove({});
        updateVotes();
    },
    'click .upvote': function() {
        node =  NodeModal.findOne();
        Meteor.call('upvote', node.title,node.parent, function(error, result) {
            if (error)
            {
                return throwError(error.reason);
            }
            displayAlert("You upvoted link: "+node.title+" in relation to "+node.parent,"info");
            NodeModal.update({},{$set:{downvoted: false,upvoted: true},$inc: {votes: 1}});
        });
    },
    'click .downvote': function() {
        node =  NodeModal.findOne();
        Meteor.call('downvote', node.title,node.parent, function(error, result) {
            // display the error to the user and abort
            if (error)
            {
                return throwError(error.reason);
            }
            displayAlert("You downvoted link: "+node.title+" in relation to "+node.parent,"info");
            NodeModal.update({},{$set:{downvoted: true,upvoted: false},$inc: {votes: -1}});
        });
    }
});