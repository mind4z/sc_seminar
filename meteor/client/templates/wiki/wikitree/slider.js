Template.slider.rendered = function () {
    $('#zoom-slider').slider({id: 'zoom-slider-wiki'});
    $('.slider-vertical').hide();
}

Template.slider.events({
    'slide #zoom-slider': function(e) {
        zoomWiki(d3.select('#zoom-slider').property('value'));
    }
});
