initDrag = function(){
    d3.behavior.drag()
        .origin(function(d) { return d; })
        .on('dragstart', dragStarted)
        .on('drag', dragged)
        .on('dragend', dragEnded);
}

function dragStarted(d) {
    d3.event.sourceEvent.stopPropagation();
    d3.select(this).classed('dragging', true);
}

function dragged(d) {
    d3.select(this).attr('cx', d.x = d3.event.x).attr('cy', d.y = d3.event.y);
}

function dragEnded(d) {
    d3.select(this).classed('dragging', false);
}