// Local (client-only) links
//NodesToDraw = new Mongo.Collection(null);

// Global variables
var width,
    height,
    root,
    duration = 750,
    rectW = 90,
    rectH = 25,
    maxCharacters = 12,
    tree,
    svg,
    id_counter = 0,
    maxWeight,
    maxVotes,
    zoom;

displayTopic = function(topic) {
    maxWeight = 0;
    maxVotes = 0;
    width = $(window).width();
    height = $(window).height() * 0.8;

    root = topic;

    $('.slider-vertical').show();
    tree = d3.layout.tree();
    if(!tree) {
        tree = d3.layout.tree().nodeSize([rectW+5, rectH+10]);
    } else {
        tree.nodeSize([rectW+5, rectH+10]);
    }


     svg = d3.select('.graph');
     if(svg.size() == 1) {
        svg.select('svg').remove();
    }

    zoom = d3.behavior.zoom()
        .scaleExtent([0.1, 3])
        .on('zoom', function() {
            svg.attr('transform', 'translate(' + d3.event.translate + ') scale(' + d3.event.scale + ')');
            $('#zoom-slider').slider('setValue', d3.event.scale);
        });

    svg = d3.select('.graph').append('svg')
        .attr('width', width)
        .attr('height', height)
        .attr('style', 'outline: thin solid #eee;')
        .call(zoom)
        .append('g')
        .attr('transform', 'translate(' + (width / 2 - 100) + ',' + 60 + ')');


    //necessary so that zoom knows where to zoom and unzoom from
    zoom.translate([(width / 2 - 100), 60]);

    root.x0 = 0;
    root.y0 = height / 2;


    update(root);
};

var  update = function(source) {
    // Compute the new tree layout.
    var nodes = tree.nodes(root).reverse(),
        links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach(function (node) {
        node.y = node.depth * 140;
    });

    // Update the nodes…
    var node = svg.selectAll('g.node')
        .data(nodes, function (node) {
            return node.id || (node.id = ++id_counter);
        });

    // Enter any new nodes at the parent's previous position.
    var nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr('transform', function () {
            return 'translate(' + source.x0 + ',' + source.y0 + ')';
        });

    nodeEnter.append('rect')
        .attr('class', function(node) {
            if(collapsed(node)) {
                return 'collapsed'
            } else {
                return 'expanded';
            }
        })
        .attr('width', rectW)
        .attr('height', rectH)
        .attr('rx', 10)
        .attr('ry', 10)
        .attr('stroke', '#bbb')
        .attr('stroke-width', 1)
        .style('fill', function(node){
            return collapsed(node) ? '#eee' : '#fff';
        });
    nodeEnter.append('text')
        .attr('x', rectW / 2)
        .attr('y', rectH / 2)
        .attr('dy', '.35em')
        .attr('text-anchor', 'middle')
        .text(function (d) {
            if(d.title.length > maxCharacters) {
                return d.title.substring(0, maxCharacters-3) + '...'
            } else {
                return d.title;
            }
        })
        .style('font-size', '10px')

    nodeEnter.append('circle')
        .attr('r', 10)
        .attr('cx', rectW / 2)
        .attr('cy', rectH + 10.5)
        .attr('stroke', '#eee')
        .attr('stroke-width', 1)
        .attr('fill', '#fff')

    nodeEnter.append('foreignObject')
        .attr('width', 15)
        .attr('height', 15)
        .attr('x', rectW / 2 -5)
        .attr('y', rectH + 1)

        .append('xhtml:span')
        .attr('class', function(node){
            if(collapsed(node)) {
                return 'control glyphicon glyphicon-plus';
            } else {
                return 'control glyphicon glyphicon-minus';
            }
        })
        .style('font-size', '10px');

    // Transition nodes to their new position.
    var nodeUpdate = node.transition()
        .duration(duration)
        .attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
        });

    nodeUpdate.select('rect')
        .attr('width', rectW)
        .attr('height', rectH)
        .attr('rx', 10)
        .attr('ry', 10)
        .attr('stroke', '#bbb')
        .attr('stroke-width', 1)
        .style('fill', function(node){
            return collapsed(node) ? '#eee' : '#fff';
        });

    nodeUpdate.select('text')
        .style('fill-opacity', 1);

    nodeUpdate.select('circle');

    nodeUpdate.select('foreignObject');

    nodeUpdate.select('span')
        .attr('class', function(node){
            if(collapsed(node)) {
                return 'control glyphicon glyphicon-plus';
            } else {
                return 'control glyphicon glyphicon-minus';
            }
        })
        .style('font-size', '10px')

    // Transition exiting nodes to the parent's new position.
    var nodeExit = node.exit().transition()
        .duration(duration)
        .attr('transform', function (d) {
            return 'translate(' + source.x + ',' + source.y + ')';
        })
        .remove();

    nodeExit.select('rect')
        .attr('width', rectW)
        .attr('height', rectH)
        .attr('rx', 10)
        .attr('ry', 10)
        .attr('stroke', '#bbb')
        .attr('stroke-width', 1);

    nodeExit.select('text');

    nodeExit.select('circle');

    nodeExit.select('foreignObject');

    // Update the links…
    var link = svg.selectAll('path.link')
        .data(links, function (d) {
            return d.target.id;
        });

    // Enter any new links at the parent's previous position.
    link.enter().insert('path', 'g')
        .attr('class', 'link')
        .attr('id', function(d) {
            maxWeight = calculateMaxWeight(source);
            maxVotes = calculateMaxVotes(source);

            var mode = $(".panel-body input[type='radio']:checked").val();
            if(maxVotes <= 0 || mode != "vote")
            {
                return 'link-'+Math.floor(Math.floor((100 / maxWeight) * d.target.weight)/10) * 10;
            }

            if(!d.target.votes || d.target.votes <=0){
                return 'link-'+10;
            }
            else{
                return 'link-'+Math.floor(Math.floor((100 / maxVotes) * d.target.votes)/10) * 10;
            }
        })
        .attr('x', rectW / 2)
        .attr('y', rectH / 2)
        .attr('d', function (d) {
            var o = {
                x: source.x0,
                y: source.y0
            };
            return diagonal({
                source: o,
                target: o
            });
        });

    // Transition links to their new position.
    link.transition()
        .duration(duration)
        .attr('d', diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
        .duration(duration)
        .attr('d', function (d) {
            var o = {
                x: source.x,
                y: source.y
            };
            return diagonal({
                source: o,
                target: o
            });
        })
        .remove();

    // Stash the old positions for transition.
    nodes.forEach(function (d) {
        d.x0 = d.x;
        d.y0 = d.y;
    });
};

// calculate max weight number
var calculateMaxWeight = function(source) {
    if(source.weight) {
        if(source.weight > maxWeight) {
            maxWeight = source.weight;
        }
    }
    if(source.children) {
        source.children.forEach(function(element){
            calculateMaxWeight(element);
        })
    }
    return maxWeight;
};

// calculate max votes number
var calculateMaxVotes = function(source) {
    if(source.votes) {
        if(source.votes > maxVotes) {
            maxVotes = source.votes;
        }
    }
    if(source.children) {
        source.children.forEach(function(element){
            calculateMaxVotes(element);
        })
    }
    return maxVotes;
};

var diagonal = d3.svg.diagonal()
    .projection(function (d) {
        return [d.x + rectW / 2, d.y + rectH / 2];
    });

var collapsed = function(node){
    return node.collapsed_children || ! node.hasOwnProperty('children');
}

zoomWiki = function(factor){
    zoom.scale(factor).event(svg);
}

Template.wikiTree.rendered = function () {
    initDrag();
}

Template.wikiTree.events({
    'click foreignObject': function(e) {
        node = e.target.__data__;
        if (node.children) {
            node.collapsed_children = node.children;
            node.children = false;
            update(node);
        } else {
            var empty_topic = {
                title: node.title
            };
            $('.spinner-container').show();
            Meteor.call('addTopic', empty_topic, function(error, result) {
                if (error)
                {
                    throwError(error.reason);
                }
                updateTopic = Meteor.call(
                    'findOneDisplayTopic',
                    node.title,
                    $('#links-number').val(),
                    $(".panel-body input[type='radio']:checked").val(),
                    $('#searchString').val(),
                    function(error,result){
                        node.children = result.children;
                        node.collapsed_children = false;
                        update(node);
                        $('.spinner-container').hide();
                    });
            });
        }
    }
});
Template.wikiTree.events({
    'click rect': function(e) {
        displayModal(e.target.__data__);
    },
    'click text': function(e) {
        displayModal(e.target.__data__);
    }
});

updateVotes = function(){
    var updated = false;
    var setVotes = function(node){
        var updated_node = Links.findOne({title: node.title,parent: node.parent.title});
        if(node.title=="Adolf Hitler"){
            console.log(updated_node);
            console.log(node.parent.title);
        }

        if(updated_node){
            if(node.votes != updated_node.votes){
                node.votes = updated_node.votes;
                updated = true;
            }
        }

        if(node.children) {
            node.children.forEach(function(node) {
                return setVotes(node);
            });
        }
    }
    if(root) {
        root.children.forEach(function (node) {
            setVotes(node);
        });
        if (updated) {
            var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);

            // Update the links…
            var svg_links = svg.selectAll('path.link')
                .data(links, function (d) {
                    return d.target.id;
                });

            svg_links.attr('id', function(d) {
                maxVotes = calculateMaxVotes(root);

                if(maxVotes <= 0)
                {
                    return 'link-'+Math.floor(Math.floor((100 / maxWeight) * d.target.weight)/10) * 10;
                }

                if(!d.target.votes || d.target.votes <=0){
                    return 'link-'+10;
                }
                else{
                    return 'link-'+Math.floor(Math.floor((100 / maxVotes) * d.target.votes)/10) * 10;
                }
            });
        }
    }

}

Meteor.autorun(function() {
    var nr_vores = Votes.find({}).fetch();
    updateVotes();
});