Template.wikiForm.created = function() {
    Session.set('submitTopicErrors', {});
}

Template.wikiForm.helpers({
    errorMessage: function(field) {
        return Session.get('submitTopicErrors')[field];
    },
    errorClass: function (field) {
        return !!Session.get('submitTopicErrors')[field] ? 'has-error' : '';
    }
});

var capitalise = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

var submitTopic = function(){
    var topic = {
        title: capitalise($('#topic-title').val())
    };

    var errors = validateTopic(topic);
    if (errors.title){
        return Session.set('submitTopicErrors', errors);
    }
    Session.set('submitTopicErrors', {});


    $('.spinner-container').show();
    Meteor.call('addTopic', topic, function(error, result) {
        // display the error to the user and abort
        $('.spinner-container').hide();
        if (error)
        {
            throwError(error.reason);
            return;
        }


        // show this result but route anyway
        if (result.notExists)
            return Session.set('submitTopicErrors', 'This topic has not already been posted');

        topic = Meteor.call('findOneDisplayTopic',
            topic.title,
            $('#links-number').val(),
            $(".panel-body input[type='radio']:checked").val(),
            $('#searchString').val(),
            function(error,result){
                $('#collapseForm').collapse('hide');
                displayTopic(result);
            });
    });
}
Template.wikiForm.events({
    'click #topic-submit': function() {
        submitTopic();
    },
    'keypress input':  function(event) {
        if (event.which == 13) {
            submitTopic();
        }
    }
});