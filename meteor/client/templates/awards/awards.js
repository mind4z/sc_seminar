Template.awards.helpers({
    awardVotes: function() {
        if(this.nrVotes == 0){
            return null;
        }
        else if(this.nrVotes > 0 && this.nrVotes < 2){
            return "bronze";
        }
        else if(this.nrVotes >= 2 && this.nrVotes < 3){
            return "silver";
        }
        else{
            return "gold"
        }
    },
    awardComments: function(){
        if(this.nrComments == 0){
            return null;
        }
        else if(this.nrComments > 0 && this.nrComments < 2){
            return "bronze";
        }
        else if(this.nrComments >= 2 && this.nrComments < 3){
            return "silver";
        }
        else{
            return "gold"
        }
    }
});
