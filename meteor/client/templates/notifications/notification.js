Template.notification.events({
    'click a': function() {
        node = Links.findOne({title:this.topicTitle,parent:this.topicParent});
        displayModal(node);
        Notifications.update(this._id, {$set: {read: true}});

    }
});
Template.notification.helpers({
    isComment: function() {
        return this.type == "comment";
    }
});