Template.notifications.helpers({
    notifications: function() {
        return Notifications.find({user:Meteor.userId(),read:false});
    },
    notificationCount: function(){
        return Notifications.find({user:Meteor.userId(),read:false}).count();
    }
});

