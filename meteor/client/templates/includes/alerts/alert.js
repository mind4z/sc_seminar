Template.alert.helpers({
    classes: function() {
        return 'alert alert-' + this.level + ' alert-dismissible';
    }
});

Template.alert.rendered = function() {
    var alert = this.data;
    if(alert.dismiss != -1){
        Meteor.setTimeout(function () {
            Alerts.remove(alert._id);
        }, alert.dismiss);
    }
};