// Local (client-only) collection
Alerts = new Mongo.Collection(null);

displayAlert = function(message, level, dismiss) {
    if(!dismiss) {
        dismiss = 3000;
    }
    var levels = ['success', 'info', 'warning', 'danger'];
    if(levels.indexOf(level) == -1) {
        level = 'warning';
    }
    Alerts.insert({message: message, level: level, dismiss: dismiss});
};

Template.alerts.helpers({
    alerts: function() {
        return Alerts.find();
    }
});