Comments = new Mongo.Collection('comments');

Meteor.methods({
    addComment: function(comment) {
        //check(this.userId, String);
        /**
        check(commentAttributes, {
            postId: String,
            body: String
        });**/
        var user = Meteor.user();
        var link = Links.findOne({title: comment.topicTitle, parent: comment.topicParent});
        if (!link){
            throw new Meteor.Error('invalid-comment', 'You must comment on a topic');

        }
        if (!user){
            throw new Meteor.Error('invalid-comment', 'You must be logged  in to comment on a link');

        }
        comment = _.extend(comment, {
            userId: user._id,
            author: user.emails[0].address,
            submitted: new Date()
        });

        // create the comment, save the id
        comment._id = Comments.insert(comment);
        createCommentNotification(comment);
        // now create a notification, informing the user that there's been a comment
        //createCommentNotification(comment);
        return comment._id;
    }
});