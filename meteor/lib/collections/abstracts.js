Abstracts = new Mongo.Collection('abstracts');

Meteor.methods({
    addAbstract: function(title) {

        var abstract = Abstracts.findOne({title: title});
        if (abstract) {
            return abstract;
        }

        if(Meteor.isServer){
            var wikiAbstract = Wiki.getAbstract(title);
            if(title && wikiAbstract.abstract)
            {
                Abstracts.insert(wikiAbstract);
                return wikiAbstract;
            }
            else{
                no_abstract = {title: title, abstract: "No Abstract available"};
                Abstracts.insert(no_abstract);
                return no_abstract;
            }

        }
    }
});