Notifications = new Mongo.Collection('notifications');

Notifications.allow({
    update: function(userId, doc, fieldNames) {
        return doc && doc.user === userId && fieldNames[0] === 'read';
    }
});

createCommentNotification = function(comment) {
    interesters = Topics.findOne({title :comment.topicParent}).interesters;
    interesters.forEach(function (interester) {
        if(interester != Meteor.user()._id)
        {
            Notifications.insert({
                user: interester,
                commenterName: comment.author,
                topicTitle: comment.topicTitle,
                topicParent:comment.topicParent,
                type: "comment",
                date: new Date(),
                read: false
            });
        }

    });

};

createVotingNotification = function(title,parent) {
    interesters = Topics.findOne({title :parent}).interesters;
    console.log(interesters);
    interesters.forEach(function (interester) {
        if(interester != Meteor.user()._id) {
            Notifications.insert({
                user: interester,
                topicTitle: title,
                topicParent: parent,
                type: "voting",
                date: new Date(),
                read: false
            });
        }
    });
};