Links = new Mongo.Collection('links');

Meteor.methods({
    getUpvotedByCurrentUser: function(topicTitle, topicParent){
        return Links.findOne({title: topicTitle, parent: topicParent, upvoters:{$in:[this.userId]}});
    },
    getDownvotedByCurrentUser: function(topicTitle, topicParent){
        return Links.findOne({title: topicTitle, parent: topicParent, downvoters:{$in:[this.userId]}});
    },
    upvote: function(topicTitle, topicParent) {
        var user = Meteor.user();
        var link = Links.findOne({title: topicTitle, parent: topicParent});
        if (!link){
            throw new Meteor.Error('invalid-comment', 'You must vote on a topic');

        }
        if (!user){
            throw new Meteor.Error('invalid-comment', 'You must be logged  in to vote on a link');

        }

        var affected = Links.update({
            title: topicTitle,
            parent: topicParent,
            upvoters: {$ne: user._id}
        }, {
            $pull: {downvoters: this.userId},
            $addToSet: {upvoters: this.userId},
            $inc: {votes: 1}
        });
        if (! affected){
            throw new Meteor.Error('invalid', "You can only upvote a topic once");
        }
        else{
            Votes.update({},{$inc: {nrVotes: 1}});
            createVotingNotification(topicTitle,topicParent);
        }

    },
    downvote: function(topicTitle, topicParent) {
        var user = Meteor.user();
        var link = Links.findOne({title: topicTitle, parent: topicParent});
        if (!link){
            throw new Meteor.Error('invalid-comment', 'You must vote on a topic');

        }
        if (!user){
            throw new Meteor.Error('invalid-comment', 'You must be logged  in to vote on a link');

        }

        var affected = Links.update({
            title: topicTitle,
            parent: topicParent,
            downvoters: {$ne: user._id}
        }, {
            $pull: {upvoters: this.userId},
            $addToSet: {downvoters: this.userId},
            $inc: {votes: -1}
        });
        if (! affected){
            throw new Meteor.Error('invalid', "You can only downvote a topic once");
        }
        else{
            Votes.update({},{$inc: {nrVotes: 1}});
            createVotingNotification(topicTitle,topicParent);
        }
    }
});