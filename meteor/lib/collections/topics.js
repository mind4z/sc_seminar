Topics = new Mongo.Collection('topics');

Meteor.methods({
    addTopic: function(topic) {
        //check(Meteor.userId(), String);
        //check(topic.title, String);
        var user = Meteor.user();

         var topicWithSameTitle = Topics.findOne({title: topic.title});
         if (topicWithSameTitle) {
             if(user){
                 Topics.update({
                     _id: topicWithSameTitle._id,
                     interesters: {$ne: user._id}
                 }, {
                     $addToSet: {interesters: user._id},
                     $inc: {interests: 1}
                 });
            }
            return {
                topicExists: true,
                topic: topicWithSameTitle

            }
        }

        if(Meteor.isServer){
            var wikiLinks = Wiki.getLinks(topic.title);
            if(wikiLinks.error){
                throw new Meteor.Error(wikiLinks.error.type, wikiLinks.error.reason);
            }
            if(topic.title && wikiLinks)
            {
                topic.title = wikiLinks.title;
                topic.pageId = wikiLinks.pageId;
                topic.url = 'http://en.wikipedia.org/wiki/'+topic.title.replace(/ /g,"_");
                topic.commentsCount = 0;

                topic.maxBacklink = 0;
                topic.maxLength = 0;
                topic.maxWatchers = 0;

                wikiLinks.links.forEach(function(link){
                    if(link.length > topic.maxLength){
                        topic.maxLength = link.length;
                    }
                    if(link.backlink > topic.maxBacklink){
                        topic.maxBacklink = link.backlink;
                    }
                    if(link.watchers > topic.maxWatchers){
                        topic.maxWatchers = link.watchers;
                    }
                });

                wikiLinks.links.forEach(function(link){
                    var lengthWeight = (topic.maxLength == 0 ? 1 : link.length/topic.maxLength);
                    var backlinkWeight = (topic.maxBacklink == 0 ? 1 : link.backlink/topic.maxBacklink);
                    var watchersWeight = (topic.maxWatchers == 0 ? 1 : link.watchers/topic.maxWatchers);
                    var sementicsWeight = (link.semantic == "" ? 0 : 1);

                    var weight = (lengthWeight+backlinkWeight+watchersWeight+sementicsWeight)/4;

                    var link = {
                        parent: topic.title,
                        title: link.title,
                        length: link.length,
                        backlink: link.backlink,
                        watchers: link.watchers,
                        semantic: link.semantic,
                        weight: weight,
                        url : 'http://en.wikipedia.org/wiki/'+link.title.replace(/ /g,"_"),
                        commentsCount: 0,
                        upvoters: [],
                        downvoters: [],
                        votes: 0,
                        random: Math.random()
                    };
                    Links.insert(link);
                });
                topic.interesters = [];
                topic.interests = 0;
                if(user){
                    topic.interesters.push(user._id);
                    topic.interests = 1;
                }

            }

        }

        var topic = _.extend(topic, {
            submitted: new Date()
        });

        var topic = Topics.insert(topic);

        return {
            _id: topic
        };
    },

    findOneDisplayTopic: function(title, nrChildren, mode, search_string){
        nrChildren = Number(nrChildren);
        if(!nrChildren){
            nrChildren = 5;
        }
        var topic = Topics.findOne({title: title});
        var search = new RegExp(search_string, 'i');
        if(mode == "vote"){
            children = Links.find({parent: title,  title: search },{sort: {votes: -1, weight:-1},limit: nrChildren}).fetch();
        }
        else if(mode == "auto"){
            children = Links.find({parent: title},{sort: {weight:-1},limit: nrChildren}).fetch();
        }
        else{
            var rand = Math.random();
            console.log(rand);
            children = Links.find({parent: title,random : { $gte : rand } },{sort: {random: 1},limit: nrChildren}).fetch();
            console.log(children);
            if(!children || children.length<nrChildren){
                nrChildren = nrChildren-children.length;
                children = Links.find({parent: title,random : { $lt : rand } },{sort: {random: -1},limit: nrChildren}).fetch();
            }
        }

        topic.children = children;

        return topic;
    }

});

validateTopic = function (topic) {
    var errors = {};
    if (!topic.title){
        errors.title = "Please enter a Topic to explore";
    }
    return errors;
}