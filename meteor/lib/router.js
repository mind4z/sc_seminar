Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound',
    waitOn: function() {
        Meteor.subscribe('topics');
        Meteor.subscribe('links');
        Meteor.subscribe('votes');
        Meteor.subscribe('comments');
        Meteor.subscribe('abstracts');
        Meteor.subscribe('notifications');
    }
});
WikiController = RouteController.extend({
    template: 'wiki'
});

Router.route('/', {
    onBeforeAction: function () {
        Router.go('/wiki');
    }

});


Router.route('/wiki', {
    name: 'wiki',
    controller: WikiController
});

Router.route('/comments', {
    name: 'comments',
    data: function() {
        return {
            comments: Comments.find({userId:Meteor.userId()},{sort:{submitted: -1}})
        }
    },
    template: 'personalComments',
});

Router.route('/awards', {
    name: 'awards',
    data: function() {
        return {
            nrComments: Comments.find({userId:Meteor.userId()}).count(),
            nrVotes: Links.find({$or:
                [{upvoters:{$in:[Meteor.userId()]}},
                    {downvoters:{$in:[Meteor.userId()]}}]}
            ).count()
        }
    },
    template: 'awards',
});

var requireLogin = function() {
    if (! Meteor.user()) {
        if (Meteor.loggingIn()) {
            this.render(this.loadingTemplate);
        } else {
            this.render('accessDenied');
        }
    } else {
        this.next();
    }
}
/**
Router.onBeforeAction('dataNotFound', {only: 'postPage'});
Router.onBeforeAction(requireLogin, {only: 'postSubmit'});**/
