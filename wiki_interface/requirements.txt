Flask==0.10.1
Jinja2==2.7.3
MarkupSafe==0.23
SPARQLWrapper==1.6.4
Werkzeug==0.9.6
isodate==0.5.1
itsdangerous==0.24
pyparsing==2.0.3
requests==2.4.3
simplejson==3.6.4
