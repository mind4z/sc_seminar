import re
import requests
import simplejson as json

from flask import Flask, jsonify
from SPARQLWrapper import SPARQLWrapper, JSON
from threading import Thread

import sys
if sys.version_info > (2, 8):
    from queue import Queue
else:
    from Queue import Queue

app = Flask(__name__)

# creates a list of list slices
chunks = lambda l, n: [l[x: x+n] for x in range(0, len(l), n)]

q = Queue()

sparql = SPARQLWrapper("http://dbpedia.org/sparql")

def worker():
    while True:
        arguments = q.get()
        get_meta_data(arguments[0], arguments[1])
        q.task_done()

def get_semantics(title, result):
    sparql.setQuery(""" Select *
                        WHERE {
                            {<http://dbpedia.org/resource/%s> ?p ?o .}
                            } """ % (title.replace(" ","_")))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    res = res["results"]["bindings"]

    result["semantics"] = []

    for triple in res:
        result["semantics"].append({"predicate": triple["p"]["value"].rsplit('/',1)[-1], "object": triple["o"]["value"].rsplit('/',1)[-1]})

def get_abstract(title):
    abstract = {}
    abstract['title'] = title;
    sparql.setQuery(""" SELECT ?abstract
                      WHERE {
                         {<http://dbpedia.org/resource/%s>  <http://dbpedia.org/ontology/abstract> ?abstract .
                         FILTER langMatches( lang(?abstract), 'en') }
                  }""" % (title.replace(" ","_")))
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    return res["results"]["bindings"][0]["abstract"]["value"]

# Parts taken from the API documentation of Wikipedia
def query_wikipedia(request):
    request['action'] = 'query'
    request['format'] = 'json'
    lastContinue = {'continue': ''}
    counter = 0
    while counter < 10:
        # Clone original request
        req = request.copy()
        # Modify it with the values returned in the 'continue' section of the last result.
        req.update(lastContinue)
        # Call API
        result = requests.get('http://en.wikipedia.org/w/api.php', params=req).json()
        robj = re.search("^(.*)continue$", "\n".join(result.keys()), re.MULTILINE)
        if 'error' in result: raise Error(result['error'])
        if 'warnings' in result: print(result['warnings'])
        if 'query' in result: yield result['query']
        if robj is None: break
        lastContinue = result[robj.group()]
        counter += 1

def get_backlinks(title, return_value):
    backlinks = 0 
    res = {}

    for result in query_wikipedia({'bltitle':title, 'list':'backlinks', 'bllimit':500, 'blfilterredir':'nonredirects', 'blnamespace':0}):
        if 'backlinks' in return_value.keys():
            return_value['backlinks'].extend(result['backlinks'])
        else:
            return_value['backlinks'] = result['backlinks']

def get_meta_data(title, return_value):
    for result in query_wikipedia({'titles':title, 'prop':'info', 'inprop':'watchers'}):
        for link in result[list(result.keys())[0]].values():
            return_value[link['title']] = {}

            return_value[link['title']]['watchers'] = link.get('watchers',0)
            return_value[link['title']]['length'] = link.get('length',0)

@app.route('/abstract/<title>', methods=['GET'])
def get_semantic(title):
    abstract = get_abstract(title)
    result = {"abstract": abstract, "title": title}
    return jsonify(result)

@app.route('/wiki/<title>', methods=['GET'])
def get_links(title):
    meta = {}
    links_result = {}
    abstracts = {}

    # get all links from the queried article
    for result in query_wikipedia({'titles':title, 'prop':'links', 'plnamespace':0, 'pllimit':500}):
        if 'links' in meta.keys():
            meta['links'].extend(result[list(result.keys())[0]][list(result[list(result.keys())[0]])[0]]['links'])
        else:
            meta.update(result[list(result.keys())[0]][list(result[list(result.keys())[0]])[0]])

    # get all backlinks for the queried article (threaded)
    backlinks = {}
    backlink_thread = Thread(target=get_backlinks, args=(title, backlinks))
    backlink_thread.start()

    semantics = {}
    semantics_thread = Thread(target=get_semantics, args=(title, semantics))
    semantics_thread.start()

    links = []

    # remove oneself from list
    meta['links'] = [x for x in meta['links'] if not x['title'] == title]

    # prepare the links for meta data querying
    del meta['ns']
    for link in meta['links']:
        del link['ns']
        links.append(link['title'])

    # make chunks of 50 titles per list to query multiple articles together
    links = chunks(links, 50)

    # add the parameters and the result list to a queue for the threads
    for linklist in links:
        q.put(("|".join(linklist), links_result))

    # wait for all threads to finish
    q.join()
    backlink_thread.join()
    semantics_thread.join()

    semantic_list = []

    for link in meta['links']:
        link['watchers'] = links_result[link['title']]['watchers']
        link['length'] = links_result[link['title']]['length']
        link['backlink'] = 1 if any(d.get('title', None) == link['title'] for d in backlinks['backlinks']) else 0
        if 'semantics' in semantics.keys():
            semantic_list = [d.get('predicate') for d in semantics['semantics'] if d.get('object', None).replace("_"," ") == link['title']]
            link['semantic'] = semantic_list[0] if len(semantic_list) > 0 else ""
        else:
            link['semantic'] = "";

    return jsonify(meta)

if __name__ == "__main__":
    for i in range(10):
        t = Thread(target=worker)
        t.daemon = True
        t.start()
    app.run(debug=True)
